﻿using ExpenseTracker.Repository.Entities;
using System.Collections.Generic;
using System.Linq;

namespace ExpenseTracker.Repository.Factories
{
    public class ExpenseGroupFactory
    {
        public ExpenseGroupFactory()
        {
        }

        public ExpenseGroup CreateExpenseGroup(DTO.ExpenseGroup expenseGroup)
        {
            return new ExpenseGroup()
            {
                Description = expenseGroup.Description,
                ExpenseGroupStatusId = expenseGroup.ExpenseGroupStatusId,
                Id = expenseGroup.Id,
                Title = expenseGroup.Title,
                UserId = expenseGroup.UserId,
                Expenses = expenseGroup.Expenses == null ? new List<Expense>() : expenseGroup.Expenses.Select(e => _expenseFactory.CreateExpense(e)).ToList()
            };
        }

        public DTO.ExpenseGroup CreateExpenseGroup(ExpenseGroup expenseGroup)
        {
            return new DTO.ExpenseGroup()
            {
                Description = expenseGroup.Description,
                ExpenseGroupStatusId = expenseGroup.ExpenseGroupStatusId,
                Id = expenseGroup.Id,
                Title = expenseGroup.Title,
                UserId = expenseGroup.UserId,
                Expenses = expenseGroup.Expenses.Select(e => _expenseFactory.CreateExpense(e)).ToList()
            };
        }

        private ExpenseFactory _expenseFactory = new ExpenseFactory();
    }
}